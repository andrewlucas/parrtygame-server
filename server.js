const express = require("express");
const app = express();
const server = require("http").Server(app);
const uuid4 = require("uuid4");

app.use("/", (req, res) => {
  res.send("Hello World");
});

// define socket.io with allow origin
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});

server.listen(5000, () => {
  console.log("Server listening on port 5000");
});

function generate4letterPIN() {
  let pin = Math.floor(1000 + Math.random() * 9000);
  if (rooms[pin]) {
    return generate4letterPIN();
  }
  return pin.toString();
}

const rooms = {
  
};
const players = {

};

io.on("connection", (socket) => {
  console.log("New connection", socket.id);
  if (!socket.handshake.auth.token) {
    const token = uuid4();
    socket.handshake.auth.token = token;
    socket.emit("token", {
      token: token,
    });
    socket.join(token);
  } else {
    const token = socket.handshake.auth.token;
    socket.join(token);
  }

  // if host find or create room
  if (socket.handshake.auth.isHost) {
    const room = Object.entries(rooms).find(
      ([key, value]) => value === socket.handshake.auth.token
    );
    if (room) {
      socket.join(room[0]);
      socket.emit("room", room[0]);
    } else {
      const room = generate4letterPIN();
      rooms[room] = socket.handshake.auth.token;
      socket.join(room);
      socket.emit("room", room);
    }
  }

  socket.on("joinRoom", (payload, callback) => {
    const { room } = payload;
    if (!rooms[room]) {
      return callback({ error: "room-not-found" });
    }

    socket
      .to(rooms[room])
      .emit("player", { id: socket.handshake.auth.token, ...payload });
    players[socket.handshake.auth.token] = room;
    return callback({});
  });

  socket.on("message", (payload) => {
    io.to(payload.to).emit("message", {
      ...payload,
      from: socket.handshake.auth.token,
    });
  });

  socket.on("disconnect", () => {
    if(!players[socket.handshake.auth.token]) return;
    io.to(rooms[players[socket.handshake.auth.token]]).emit(
      "player-disconnected",
      { id: socket.handshake.auth.token }
    );
  });
});
